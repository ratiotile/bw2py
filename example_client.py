from bw2py import BWAPI
from time import sleep
import ipdb

client = BWAPI.cvar.BWAPIClient


def reconnect():
    while not client.connect():
        sleep(1)

def showPlayers():
    players = Broodwar.getPlayers()
    for player in players:
        pass  #Broodwar << "Player [" << player.getID() << "]: " << player.getName() << " is in force: " << player.getForce().getName() << "\n"

def showForces():
    forces = Broodwar.getForces()
    for force in forces:
        players = force.getPlayers()
        pass  #Broodwar << "Force " << force.getName() << " has the following players:\n"
        for player in players:
            pass  #Broodwar << "  - Player [" << player.getID() << "]: " << player.getName() << "\n"

def drawStats():
    line = 0
    allUnitTypes = BWAPI.UnitTypes.allUnitTypes()
    Broodwar.drawTextScreen(BWAPI.Position(5,0), "I have "+str(Broodwar.self().allUnitCount())+" units:")
    for unitType in allUnitTypes:
        count = Broodwar.self().allUnitCount(unitType)
        if count>0:
            line += 1
            statStr = "- "+str(count)+" "+str(unitType)
            Broodwar.drawTextScreen(BWAPI.Position(5, 12*line), statStr)

def drawBullets():
    bullets = Broodwar.getBullets()
    for bullet in bullets:
        p = bullet.getPosition()
        velocityX = bullet.getVelocityX()
        velocityY = bullet.getVelocityY()
        lineColor = BWAPI.Colors.Red
        textColor = BWAPI.Text.Red
        if bullet.getPlayer == Broodwar.self():
            lineColor = BWAPI.Colors.Green
            textColor = BWAPI.Text.Green
        Broodwar.drawLineMap(p, p+BWAPI.Position(velocityX,velocityY), lineColor)
        Broodwar.drawTextMap(p, chr(textColor) + str(bullet.getType()))

def drawVisibilityData():
    wid = Broodwar.mapWidth()
    hgt = Broodwar.mapHeight()
    for x in range(wid):
        for y in range(hgt):
            drawColor = BWAPI.Colors.Red
            if Broodwar.isExplored(tileX=x,tileY=y):
                if Broodwar.isVisible(tileX=x,tileY=y):
                    drawColor = BWAPI.Colors.Green
                else:
                    drawColor = BWAPI.Colors.Blue

            Broodwar.drawDotMap(BWAPI.Position(x*32+16,y*32+16), drawColor)

print("Connecting...")
reconnect()
Broodwar = BWAPI.cvar.Broodwar  # is None if set above this line
while True:
    print("waiting to enter match")
    #Broodwar = BWAPI.cvar.Broodwar  # reset reference

    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")
    Broodwar.sendText( "Hello world from python!"); #bug, doesn't print
    Broodwar.printf( "Hello world from python!");
    # need newline to flush buffer
    pass  #Broodwar << #"The map is " << Broodwar.mapName() << ", a " \
    #    << len(Br#oodwar.getStartLocations()) << " player map" << " \n"

    # Enable some cheat flags

    Broodwar.enableFlag(BWAPI.Flag_UserInput);

    show_bullets=False;
    show_visibility_data=False;

    if Broodwar.isReplay():
        pass  #Broodwar << "The following players are in this replay:\n"
        players = Broodwar.getPlayers()
        #TODO add rest of replay actions

    else:
        ## test section
        print ("mapwidth " + str(Broodwar.mapWidth()))
        print ("mapheight " + str(Broodwar.mapHeight()))
        ## test section
        pass  #Broodwar << "The matchup is " << Broodwar.self().getRace() << " vs " << Broodwar.enemy().getRace() << "\n"
        #send each worker to the mineral field that is closest to it
        units    = Broodwar.self().getUnits();
        minerals  = Broodwar.getMinerals();
        ipdb.set_trace()
        print("got", units.__sizeof__(), "units")
        print("got", minerals.__sizeof__(), "minerals")
        for unit in units:
            if unit.getType().isWorker():
                closestMineral = None
                #print("worker")
                for mineral in minerals:
                    if closestMineral is None or unit.getDistance(mineral) < unit.getDistance(closestMineral):
                        closestMineral = mineral
                if closestMineral:
                    unit.rightClick(closestMineral)
            elif unit.getType().isResourceDepot():
                unit.train(Broodwar.self().getRace().getWorker())
        events = Broodwar.getEvents()
        print(len(events))

    while Broodwar.isInGame():
        events = Broodwar.getEvents()
        for e in events:
            eventtype = e.getType()
            if eventtype == BWAPI.EventType.MatchEnd:
                if e.isWinner():
                    pass  #Broodwar << "I won the game\n"
                else:
                    pass  #Broodwar << "I lost the game\n"
            elif eventtype == BWAPI.EventType.SendText:
                if e.getText() == "/show bullets":
                    show_bullets = not show_bullets;
                elif e.getText() == "/show players":
                    showPlayers()
                elif e.getText() == "/show forces":
                    showForces()
                elif e.getText()=="/show visibility":
                    show_visibility_data = not show_visibility_data
                else:
                    pass  #Broodwar << "You typed \"" << e.getText() << "\"!\n"
            elif eventtype == BWAPI.EventType.ReceiveText:
                pass  #Broodwar << e.getPlayer().getName() << " said \"" << e.getText() << "\"\n"
            elif eventtype == BWAPI.EventType.PlayerLeft:
                pass  #Broodwar << e.getPlayer().getName() << " left the game.\n"
            elif eventtype == BWAPI.EventType.NukeDetect:
                if e.getPosition() is not BWAPI.Positions.Unknown:
                    Broodwae.drawCircleMap(e.getPosition(), 40, BWAPI.Colors.Red, True)
                    pass  #Broodwar << "Nuclear Launch Detected at " << e.getPosition() << "\n"
                else:
                    pass  #Broodwar << "Nuclear Launch Detected.\n"
            elif eventtype == BWAPI.EventType.UnitCreate:
                if not Broodwar.isReplay():
                    pass  #Broodwar << "A " << e.getUnit() << " has been created at " << e.getUnit().getPosition() << "\n"
                else:
                    if e.getUnit().getType().isBuilding() and (e.getUnit().getPlayer().isNeutral() == false):
                        seconds = Broodwar.getFrameCount()/24
                        minutes = seconds/60
                        seconds %= 60
                        Broodwar.sendText(str(minutes)+":"+str(seconds)+": "+e.getUnit().getPlayer().getName()+" creates a "+str(e.getUnit().getType())+"\n")
            elif eventtype == BWAPI.EventType.UnitDestroy:
                if not Broodwar.isReplay():
                    pass  #Broodwar << "A " << e.getUnit() << " has been destroyed at " << e.getUnit().getPosition() << "\n"
            elif eventtype == BWAPI.EventType.UnitMorph:
                if not Broodwar.isReplay():
                    pass  #Broodwar << "A " << e.getUnit() << " has been morphed at " << e.getUnit().getPosition() << "\n"
                else:
                    #if we are in a replay, then we will print out the build order
                    #(just of the buildings, not the units).
                    if e.getUnit().getType().isBuilding() and not e.getUnit().getPlayer().isNeutral():
                        seconds = Broodwar.getFrameCount()/24
                        minutes = seconds/60
                        seconds %= 60
                        pass  #Broodwar << str(minutes) << ":" << str(seconds) << ": " << e.getUnit().getPlayer().getName() << " morphs a " << e.getUnit().getType() << "\n"
            elif eventtype == BWAPI.EventType.UnitShow:
                if not Broodwar.isReplay():
                    pass  #Broodwar << e.getUnit() << " spotted at " << e.getUnit().getPosition() << "\n"
            elif eventtype == BWAPI.EventType.UnitHide:
                if not Broodwar.isReplay():
                    pass  #Broodwar << e.getUnit() << " was last seen at " << e.getUnit().getPosition() << "\n"
            elif eventtype == BWAPI.EventType.UnitRenegade:
                if not Broodwar.isReplay():
                    pass  #Broodwar << e.getUnit() << " is now owned by " << e.getUnit().getPlayer() << "\n"
            elif eventtype == BWAPI.EventType.SaveGame:
                pass  #Broodwar << "The game was saved to " << e.getText() << "\n"
        if show_bullets:
            drawBullets()
        if show_visibility_data:
            drawVisibilityData()
        drawStats()
        Broodwar.drawTextScreen(BWAPI.Position(300,0),"FPS: " + str(Broodwar.getAverageFPS()))
        client.update()

