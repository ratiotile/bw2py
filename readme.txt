Setup for compiling

1. Copy BWAPI 3.7.4 include and lib folders into bw2py dir.
/bw2py
    /BWAPI-3.7.4
        BWAPI.h
        /include
            *.h
        /lib
            *.lib

2. Install SWIGwin 3.0.2 (prebuilt exe) and create env vars
PYTHON_INCLUDE and PYTHON_LIB as described in:
  http://www.swig.org/Doc3.0/Windows.html#Windows_examples
Add the dir you put SWIG.exe to your PATH.

3. Set up compiler
BWAPI 3.7.4 is compiled with vs2008 = sdk 7.0 as 32-bit,
so we need to use the version of python which used the same compiler,
Python 3.2 x86.

Install Microsoft Windows SDK for Windows 7 and .Net Framework 3.5 SP1
	http://www.microsoft.com/en-us/download/details.aspx?id=3138

Set the environment variable to tell Distutils to use Windows SDK compiler.
Run SetEnv to configure Win 7.0 SDK for 32-bit and release mode.
Open up the Win 7.0 SDK shell
	(start/programs/Microsoft Windows SDK v7.0/CMD Shell)
and type:
	set DISTUTILS_USE_SDK=1
	setenv /x86 /release
From now on, do everything on the command line from the sdk enviornment.
Either start from the shell shortcut mentioned above or use start_console.bat.

4. Python virtualenv:
you only need to do this if python3.2 is not your default python installation
(if you run python from cmd and it doesn't say "Python 3.2.x ... x86")
In the directory under bw2py,
make virtualenvs folder and create a virtualenv called python32x86.
CD to virtualenvs and run this command, with path to v3.2 python.exe
'virtualenv -p \path\to\python32\python.exe python32x86'
Now dir structure looks like this:
/workspace(for example, name doesn't matter)
	/bw2py
	/virtualenvs
		/python32x86
			/Include
			/Lib
			...
From now on, after starting the SDK console and navigating to the project folder
or using start_console.bat, activate the virtualenv: 'activate.bat'.

4.1 If you want to use ipython and ipdb:
we need to install latest version of IPython which works with python 3.2.
Make sure you are in the virtualenv.
The prompt should say (python32x86) before current directory.
	pip install "IPython>1,<2"
	pip install ipd
If this is set up correctly, ipython.bat should work.

4.2 Optional - if you rather use IDLE:
copy /Lib/idlelib and /tcl from your main python 3.2 installation
into the virtualenv
	/python32x86
		/tcl
		/Lib
			/idlelib
If this is set up correctly, idle.bat should work.

5. Building with Distutils
start_console.bat, then 'activate.bat'.
	'python setup.py build'
To install:
	'python setup.py install'