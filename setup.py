﻿from distutils.core import setup, Extension
from distutils.command.build import build
#from distutils.command.build_ext import build_ext
#from distutils.command.build_py import build_py

class mybuild(build):
    sub_commands = [
        ("build_ext", build.has_ext_modules),
        ("build_py", build.has_pure_modules)]




def make_module(name):
    return make_module_opt(name,[])

def make_module_opt(name,linkopts):
    """ linkopts must be a list of strings """
    return Extension('bw2py._' + str(name),
            sources=[ 'src/'+str(name)+'.i'], #'src/'+str(name)+'_wrap.cxx',
            swig_opts=['-c++', '-Ibwapi-3.7.4/include', '-DSWIG_TYPE_TABLE=bw2py'],
            include_dirs = ["bwapi-3.7.4/include/"],  # path to .h file(s)
            library_dirs = ["bwapi-3.7.4/lib/","G:/Python32x86/libs"],  # path to .lib file(s)
            libraries = ['BWAPI','BWAPIClient','BWTA','tinyxml','CGAL-vc90-mt','libboost_thread-vc90-mt-1_40'
                 ,'gmp-vc90-mt','mpfr-vc90-mt','python32'],
            extra_compile_args = ["/EHsc"],
            extra_link_args = linkopts,
            )

def ltcg_module(name):
    return make_module_opt(name,["/LTCG"])

def generate_modules(nameList):
    return list(map(make_module, nameList))

BWAPI_mod = ltcg_module('BWAPI')

setup(name='bw2py',
      version='0.0.1a',
      author="ratiotile",
      author_email="eujain@gmail.com",
      url="https://bitbucket.org/ratiotile/bw2py",
      description="""swig wrappers to py for BWAPI 3.7.4""",
      ext_modules=[BWAPI_mod],
      packages=['bw2py'],
      package_dir={'bw2py': 'src'},
      package_data={'bw2py': ['*.i']},
      cmdclass={'build': mybuild}  # override to build_ext first, then copy
      )
