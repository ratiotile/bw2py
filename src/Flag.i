﻿%module Flag
%{
  #include "BWAPI/Flag.h"
%}

%rename("%(regex:/.*::.*::(.*)/Flag_\\1/)s",
  regextarget=1, fullname=1) "BWAPI::Flag::.*";


%include "BWAPI/Flag.h"