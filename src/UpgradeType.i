﻿%module UpgradeType
%import "Type.i"
%import "Race.i"
%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/DamageType.h"
    #include "BWAPI/TechType.h"
    #include "BWAPI/UpgradeType.h"
    #include "BWAPI/ExplosionType.h"
    #include "BWAPI/UpgradeType.h"
%}
%include "std_string.i"
%include "std_set.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/UpgradeTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::UpgradeTypes::.*";

%include "BWAPI/UpgradeType.h"