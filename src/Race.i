﻿%module Race
%import "Type.i"
%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/Race.h"
%}
%include "std_string.i"
%include "std_set.i"


%ignore Races::init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/Races_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::Races::.*";

%include "BWAPI/Race.h"