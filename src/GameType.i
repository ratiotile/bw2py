﻿%module GameType
%import "Type.i"
%{
	#include "BWAPI/GameType.h"
%}
%include "std_string.i"
%include "std_set.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/GameTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::GameTypes::.*";

#include "BWAPI/GameType.h"