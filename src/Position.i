﻿%module Position
%{
    #include "BWAPI/Position.h"
%}

%rename(setX) x();
%rename(setY) y();

%rename("%(regex:/.*::.*::(.*)/Positions_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::Positions::.*";

%include "BWAPI/Position.h"