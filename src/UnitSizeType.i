﻿%module UnitSizeType
%import "Type.i"
%{
    #include "BWAPI/UnitSizeType.h"
%}

%include "std_string.i"
%include "std_set.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/UnitSizeTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::UnitSizeTypes::.*";

%include "BWAPI/UnitSizeType.h"