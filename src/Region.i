﻿%module Region

%import "Position.i"

%{
    #include "BWAPI/Position.h"
    #include "BWAPI/Region.h"
%}
%include "std_set.i"

%ignore c_str;
%nodefaultctor BWAPI::Region;

%include "BWAPI/Region.h"

