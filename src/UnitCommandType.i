﻿%module UnitCommandType
%import "Type.i"
%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/WeaponType.h"
    #include "BWAPI/UnitCommandType.h"
%}
%include "std_string.i"
%include "std_set.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/UnitCommandTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::UnitCommandTypes::.*";

%include "BWAPI/UnitCommandType.h"