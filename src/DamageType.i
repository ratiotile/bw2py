﻿%module DamageType
%import "Type.i"
%{
    #include "BWAPI/DamageType.h"
%}

%include "std_string.i"
%include "std_set.i"
%ignore init;
%ignore c_str;
%rename("%(regex:/.*::.*::(.*)/DamageTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::DamageTypes::.*";

%include "BWAPI/DamageType.h"

