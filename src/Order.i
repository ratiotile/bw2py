﻿%module Order
%import "Type.i"
%{
    #include "BWAPI/Order.h"
%}

%include "std_string.i"
%include "std_set.i"
%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/Orders_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::Orders::.*";

%include "BWAPI/Order.h"
