﻿%module BWAPI

%{
	#include "BWAPI.h"
%}
%include "BWAPI.h"



%include "Flag.i"
%include "Input.i"
%include "Latency.i"
%include "EventType.i"
%include "CoordinateType.i"

%include "Position.i"
%include "TilePosition.i"

%include "Type.i"
%include "Color.i"
%include "DamageType.i"
%include "GameType.i"
%include "PlayerType.i"
%include "Race.i"
%include "Order.i"
%include "TechType.i"
%include "UnitCommandType.i"
%include "UnitSizeType.i"
%include "UnitType.i"
%include "UpgradeType.i"
%include "WeaponType.i"
%include "Error.i"


%include "Force.i"
%include "Region.i"
%include "UnitCommand.i"
%include "Unit.i"
%include "Client.i"

%include "Player.i"
%include "Event.i"
%include "Game.i"