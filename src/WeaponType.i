﻿%module WeaponType
%import "Type.i"
%import "UnitType.i"
%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/DamageType.h"
    #include "BWAPI/TechType.h"
    #include "BWAPI/UpgradeType.h"
    #include "BWAPI/ExplosionType.h"
    #include "BWAPI/WeaponType.h"
%}
%include "std_string.i"
%include "std_set.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/WeaponTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::WeaponTypes::.*";

%include "BWAPI/WeaponType.h"