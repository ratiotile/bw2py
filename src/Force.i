﻿%module Force

%{
    #include "BWAPI/Force.h"
%}

%include "std_string.i"
%include "std_set.i"

%include "BWAPI/Force.h"
