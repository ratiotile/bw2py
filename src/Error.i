﻿%module Error
%import "Type.i"
%{
    #include "BWAPI/Error.h"
%}

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/Errors_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::Errors::.*";

%include "BWAPI/Error.h"