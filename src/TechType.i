﻿%module TechType
%import "Type.i"
%import "Race.i"
%import "Order.i"
%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/WeaponType.h"
    #include "BWAPI/TechType.h"
%}
%include "std_string.i"
%include "std_set.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/TechTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::TechTypes::.*";

%include "BWAPI/TechType.h"