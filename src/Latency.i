﻿%module Latency
%inline %{
  #include "BWAPI/Latency.h"
%}

%rename("%(regex:/.*::.*::(.*)/Latency_\\1/)s",
  regextarget=1, fullname=1) "BWAPI::Latency::.*";

%include "BWAPI/Latency.h"