﻿%module Game

%import "Color.i"
%import "CoordinateType.i"
%import "Error.i"
%import "Event.i"
%import "Flag.i"
%import "GameType.i"
%import "Race.i"
%import "Region.i"
%import "Order.i"
%import "Latency.i"
%import "TilePosition.i"
%import "UnitType.i"
%import "TechType.i"
%import "UpgradeType.i"
%import "Input.i"

%{
	#include "BWAPI/Region.h"
	#include "BWAPI/GameType.h"
    #include "BWAPI/Game.h"
%}
%include "std_list.i"
%include "std_map.i"
%include "std_vector.i"
%include "std_set.i"
//%nodefaultctor BWAPI::Game;

%include "BWAPI/GameType.h"
%include "BWAPI/Game.h"