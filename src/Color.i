%module Color
%import "Type.i"
%inline %{
    #include "BWAPI/Color.h"
%}

%ignore init;

%rename("%(regex:/.*::.*::(.*)/Colors_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::Colors::.*";

%include "BWAPI/Color.h"