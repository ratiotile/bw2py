﻿%module UnitType
%import "Type.i"
%import "Race.i"
%import "UnitSizeType.i"
%{
    #include "BWAPI/Race.h"
    #include "BWAPI/TechType.h"
    #include "BWAPI/UpgradeType.h"
    #include "BWAPI/WeaponType.h"
    #include "BWAPI/UnitType.h"
%}
%include "std_string.i"
%include "std_set.i"
%include "std_map.i"

%ignore init;
%ignore c_str;

%rename("%(regex:/.*::.*::(.*)/UnitTypes_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::UnitTypes::.*";

%include "BWAPI/UnitType.h"