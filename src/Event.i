﻿%module Event
%import "Player.i"
%import "Unit.i"
%import "Position.i"

%{
    #include "BWAPI/Event.h"
%}
%include "std_string.i"
%include "BWAPI/EventType.h"
%include "BWAPI/Event.h"