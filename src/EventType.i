﻿%module EventType
%{
#include "BWAPI/EventType.h"
%}

%rename("%(regex:/.*::.*::(.*)/EventType_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::EventType::.*";

#include "BWAPI/EventType.h"