﻿%module Unit
%import "Order.i"
%import "TechType.i"
%import "UnitType.i"
%import "WeaponType.i"
%import "UpgradeType.i"
%import "Position.i"
%import "TilePosition.i"
%import "UnitCommand.i"

%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/WeaponType.h"
    #include "BWAPI/Unit.h"
%}

%include "std_list.i"

%include "std_set.i"

%ignore c_str;

%include "BWAPI/Unit.h"
%include "pyabc.i"

%template(UnitSet) std::set<BWAPI::Unit*>;
