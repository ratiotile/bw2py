﻿%module Player

%import "TilePosition.i"
%import "Race.i"
%import "TechType.i"
%import "UpgradeType.i"
%import "UnitType.i"
%import "Color.i"
%import "PlayerType.i"
%import "WeaponType.i"

%{
    #include "BWAPI/UnitType.h"
    #include "BWAPI/WeaponType.h"
    #include "BWAPI/Player.h"
%}
%include "std_string.i"
%include "std_set.i"

%ignore c_str;

%include "BWAPI/Player.h"

