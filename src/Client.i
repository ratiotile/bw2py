﻿%module Client
%{
  #include "BWAPI/Client/Client.h"
%}
%ignore data;
//%ignore BWAPIClient;

%include "BWAPI/Client/Client.h"

// %inline %{
//   namespace BWAPI{
//     extern Client* Z_Client = &BWAPI::BWAPIClient;
//     extern int Z_test = 9;
//   }

// %}

  // namespace BWAPI{
  //   extern Client* Z_Client = &BWAPI::BWAPIClient;
  //   extern int Z_test = 9;
  // }