﻿%module Type
%{
#include "BWAPI/Type.h"
%}
namespace BWAPI
{
	class Type
	{
	public:
		Type(int id);
		int getID();
	};
}