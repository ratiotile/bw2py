﻿%module CoordinateType
%{
	#include "BWAPI/CoordinateType.h"
%}

%rename("%(regex:/.*::.*::(.*)/CoordinateType_\\1/)s",
  regextarget=1, fullname=1) "BWAPI::CoordinateType::.*";

#include "BWAPI/CoordinateType.h"