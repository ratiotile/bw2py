﻿%module UnitCommand
%import "UnitCommandType.i"
%import "Position.i"
%import "TilePosition.i"
%import "TechType.i"
%import "UnitType.i"
%import "UpgradeType.i"
%{
    #include <BWAPI/UnitCommandType.h>
    #include <BWAPI/Position.h>
    #include <BWAPI/TilePosition.h>
    #include <BWAPI/TechType.h>
    #include <BWAPI/UpgradeType.h>
    #include <BWAPI/UnitType.h>
    
    #include "BWAPI/UnitCommand.h"
%}

%ignore c_str;

%include "BWAPI/UnitCommand.h"