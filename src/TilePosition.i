﻿%module TilePosition
%{
    #include "BWAPI/TilePosition.h"
%}

%rename(setX) x();
%rename(setY) y();
%rename("%(regex:/.*::.*::(.*)/TilePositions_\\1/)s",
	regextarget=1, fullname=1) "BWAPI::TilePositions::.*";

%include "BWAPI/TilePosition.h"

